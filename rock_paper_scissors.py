#This program plays a game of Rock, Paper, Scissors between two Players,
and reports both Player's scores each round."""

#The Player class is the parent class for all of the Players
in this game

moves = ['rock', 'paper', 'scissors']

import random moves

Player:
    def player __init__(self):
        self.my_move = None
        self.their_move = None
        self.score = 0

    def move(self):
        return 'rock'

    def learn(self, my_move, their_move):
        self.my_move = my_move
        self.their_move = their_move


    class RandomPlayer
        def __init__(self):
            super().__init_()
            self.behavior = "Random Player"

        def move(self):
            return random.choice(moves)

        def learn(self, my_move, their_move):
            self.my_move = my_move
            self.their_move = their_move

    class HumanPlayer
        def __init__(self):
            super().__init_()
            self.behavior = "Human Player"

        def move(self):
            while True:
                move = input('Choose a move: (rock / paper / scissors)\n').)
                if move in moves:
                    return move
                else:
                    print("That move does not exist. Please try again.")

        def learn(self, my_move, their_move):
            self.my_move = my_move
            self.their_move = their_move

    class ReflectPlayer
        def __init__(self):
            super().__init_()
            self.behavior = "Reflect Player"

        def move(self):
            if not self.their_move:
                return random.choice(moves)
            else:
                return self.their_move

        def learn(self, my_move, their_move):
            self.my_move = my_move
            self.their_move = their_move

    class CyclePlayer
        def __init__(self):
            super().__init_()
            self.behavior = "Cycle Player"

        def move(self):
            self.move = moves += 1
                return(self.move)

        def learn(self, my_move, their_move):
            self.my_move = my_move
            self.their_move = their_move

def beats(one, two, move1, move2):
    return ((one == 'rock' and two == 'scissors') or
                (one == 'scissors' and two == 'paper') or
                (one == 'paper' and two == 'rock'))


class Game:
    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2
        self.p1_score = 0
        self.p2_score = 0

    def play_round(self):
        move1 = self.p1.move()
        move2 = self.p2.move()
        print(f"Player 1: {move1}  Player 2: {move2}")
        if move1 == move2:
            print("It's a tie!")
        if beats == True:
            print("Player 1 wins.")
        else:
            print("Player 2 wins.")
        self.p1.learn(move1, move2)
        self.p2.learn(move2, move1)

    def play_game(self):
        print("Game start!")
        for round in range(3):
            print(f"Round {round}:")
            self.play_round()
        print("Game over!")

    def compute_score(self, move1, move2):
        if self.beats(move1, move2):
            self.p1_score += 1
        else:
            self.p2_score += 1
        print(f"scores
        {self.p1.behavior}:
        {self.p1_score}", f"
        {self.p2.behavior}:
        {self.p2_score}")

if __name__ == '__main__':
    game = Game(Player(), Player())
    game.play_game()
